/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.anchordao;

import com.anchor.Anchor;



/**
 *
 * @author DELL
 */
public interface AnchorDao {
    public boolean checkIfExits(String hash);
   public boolean insertAnchor(Anchor.AnchorDetail anchor);
   public Anchor.AnchorDetail getAnchorDetail(String hash);
   public boolean updateProof(String hash, String proof);
}
