/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.anchordao.impl;

import com.anchor.Anchor;
import com.anchordao.AnchorDao;
import com.utils.DBConnection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author DELL
 */
public class AnchorDaoImpl implements AnchorDao {

    DBConnection db = new DBConnection();

    @Override
    public boolean insertAnchor(Anchor.AnchorDetail anchor) {
        try {
            PreparedStatement pstmt = db.con.prepareStatement("INSERT INTO ANCHOR(nodeId,nodeHost,timestamp,anchorStatus,refId) VALUES(?,?,?,?,?)");
            pstmt.setString(1, anchor.getNodeId());
            pstmt.setString(2, anchor.getNodeHost());
            pstmt.setLong(3, anchor.getTimestamp());
            pstmt.setBoolean(4, anchor.isAnchorStatus());
            pstmt.setString(5, anchor.getRefId());
            if (pstmt.executeUpdate() > 0) {
                return true;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public Anchor.AnchorDetail getAnchorDetail(String hash) {
        try {
            PreparedStatement pstmt = db.con.prepareStatement("SELECT * FROM ANCHOR WHERE refId = ?");
            pstmt.setString(1, hash);
            ResultSet resultSet = pstmt.executeQuery();
            if (resultSet.next()) {
                Anchor.AnchorDetail anchorDetail = new Anchor.AnchorDetail();
                anchorDetail.setNodeId(resultSet.getString(1));
                anchorDetail.setNodeHost(resultSet.getString(2));
                anchorDetail.setTimestamp(resultSet.getLong(3));
                anchorDetail.setRefId(hash);
                anchorDetail.setProof(resultSet.getString(6));
                return anchorDetail;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public boolean updateProof(String hash, String proof) {
        try {
            PreparedStatement pstmt = db.con.prepareStatement("UPDATE ANCHOR set proof=? WHERE refId = ?");
            pstmt.setString(1, proof);
            pstmt.setString(2, hash);
            if (pstmt.executeUpdate() > 0) {
                return true;
            }

        } catch (SQLException ex) {
            ex.printStackTrace();

        }
        return false;
    }

    @Override
    public boolean checkIfExits(String hash) {
        try {
            PreparedStatement pstmt = db.con.prepareStatement("SELECT nodeId FROM ANCHOR WHERE refId = ?");
            pstmt.setString(1, hash);
            ResultSet resultSet = pstmt.executeQuery();
            if (resultSet.next()) {
                return true;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

}
