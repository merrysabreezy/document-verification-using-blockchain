/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.anchor;

/**
 *
 * @author DELL
 */
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonNode;
import com.google.common.collect.Sets;

import java.util.*;
public class ChainPoint implements Anchor{
    private static final String CHAIN_POINT_NODES = 
            "https://a.chainpoint.org/nodes/random," +
                    "https://b.chainpoint.org/nodes/random," +
                    "https://c.chainpoint.org/nodes/random";
    private final Map<String, String> headers = new HashMap<>(2);
    private static long nodeSelected;
    private static String nodeUrl;
    //Two Hour
    private static final long nodeExpire = 7200000;
    public ChainPoint(){
        headers.put("Content-Type", "application/json");
    }

    @Override
    public AnchorDetail anchor(final String content){
        String nodeUrl = selectNode();
        if(null == nodeUrl){
            System.out.println("Could not get node for anchoring.");
            return null;
        }
        final String hash = StringUtils.sha256(content);
        PostHashRequest request = new PostHashRequest();
        request.setHashes(Sets.newHashSet(hash));
        String payload = ThirdPartyHelper.serialize(request);
        Http http = new Http.Builder().headers(headers).payload(payload, Http.ContentType.JSON).build();
        try{
            Http.Response response = http.post(nodeUrl+"/hashes");
            if(response.getStatus() == 200){
                PostHashResponse hashResponse = ThirdPartyHelper
                        .parseJson(new TypeReference<PostHashResponse>(){}, response.getResponse());
                if(null != hashResponse && null != hashResponse.getHashes()
                        && hashResponse.getHashes().size() >= 1){
                    String nodeId = hashResponse.getHashes().get(0).getHashIdNode();
                    AnchorDetail anchorDetail = new AnchorDetail();
                    anchorDetail.setNodeHost(nodeUrl);
                    anchorDetail.setNodeId(nodeId);
                    anchorDetail.setTimestamp(System.currentTimeMillis());
                    anchorDetail.setRefId(content);
                    return anchorDetail;
                }
            }else{
                System.out.println("Error on api call {} msg: {}"+response.getStatus()+response.getResponse());
            }
        }catch (HttpException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public String getProof(final AnchorDetail anchorDetail) {
        String url = anchorDetail.getNodeHost()+"/proofs/"+anchorDetail.getNodeId();
        Map<String, String> headers = new HashMap<>(1);
        headers.put("accept","application/vnd.chainpoint.ld+json");
        Http http = new Http.Builder().headers(headers).build();
        try{
            Http.Response response = http.get(url);
            if(response.getStatus() == 200){
                JsonNode node = ThirdPartyHelper.parseJson(response.getResponse());
                if(null != node){
                    for (JsonNode btcNode : node.get(0).get("anchors_complete")) {
                        if(btcNode.textValue().equalsIgnoreCase("btc")){
                            JsonNode proofNode = node.get(0).get("proof");
                            if(null != proofNode){
                                return StringUtils.encodeBase64(StringUtils.getBytes(proofNode.toString()));
                            }
                            break;
                        }
                    }
                }
            }else{
                System.out.println("Error on api call {} msg: {}"+response.getStatus()+response.getResponse());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    private String selectNode(){
        if(null == nodeUrl){
            return getNode();
        }else if((Math.abs(System.currentTimeMillis() - nodeSelected)) > nodeExpire) {
            return getNode();
        }
        return nodeUrl;
    }

    private String getNode(){
        String url = getRandomNode();
        Http http = new Http.Builder().build();
        try {
            Http.Response response = http.get(url);
            if(response.getStatus() == 200){
                if(null != response.getResponse()){
                    List<Node> nodes =
                    ThirdPartyHelper.parseJson(new TypeReference<List<Node>>(){}, response.getResponse());
                    if(null != nodes && nodes.size() >= 1){
                        Collections.shuffle(nodes);
                        Node node = nodes.get(0);
                        nodeUrl = node.getUrl();
                        nodeSelected = System.currentTimeMillis();
                        return nodeUrl;
                    }
                }
            }else{
                System.out.println("Error on api call {} msg: {}"+response.getStatus()+ response.getResponse());
            }
        } catch (HttpException e) {
            e.printStackTrace();
        }
        return null;
    }

    private String getRandomNode(){
        List<String> nodes = Arrays.asList(CHAIN_POINT_NODES.split(","));
        Collections.shuffle(nodes);
        return nodes.get(0);
    }

    @JsonInclude(value = JsonInclude.Include.NON_NULL)
    @JsonIgnoreProperties(ignoreUnknown = true)
    private static class Proof{
        @JsonProperty("hash_id_node")
        private String nodeId;
        private String proof;

        @JsonProperty("anchors_complete")
        private Set<String> anchorsComplete;

        public String getNodeId() {
            return nodeId;
        }

        public void setNodeId(String nodeId) {
            this.nodeId = nodeId;
        }

        public String getProof() {
            return proof;
        }

        public void setProof(String proof) {
            this.proof = proof;
        }

        public Set<String> getAnchorsComplete() {
            return anchorsComplete;
        }

        public void setAnchorsComplete(Set<String> anchorsComplete) {
            this.anchorsComplete = anchorsComplete;
        }

        @Override
        public String toString() {
            return "Proof{" +
                    "nodeId='" + nodeId + '\'' +
                    ", proof='" + proof + '\'' +
                    ", anchorsComplete=" + anchorsComplete +
                    '}';
        }
    }

    @JsonInclude(value = JsonInclude.Include.NON_NULL)
    @JsonIgnoreProperties(ignoreUnknown = true)
    private static class PostHashRequest{

        private Set<String> hashes;

        public Set<String> getHashes() {
            return hashes;
        }

        public void setHashes(Set<String> hashes) {
            this.hashes = hashes;
        }

    }

    @JsonInclude(value = JsonInclude.Include.NON_NULL)
    @JsonIgnoreProperties(ignoreUnknown = true)
    private static class PostHashResponse{
        private Meta meta;
        private List<Hash> hashes;

        public Meta getMeta() {
            return meta;
        }

        public void setMeta(Meta meta) {
            this.meta = meta;
        }

        public List<Hash> getHashes() {
            return hashes;
        }

        public void setHashes(List<Hash> hashes) {
            this.hashes = hashes;
        }

        @Override
        public String toString() {
            return "PostHashResponse{" +
                    "meta=" + meta +
                    ", hashes=" + hashes +
                    '}';
        }
    }
    @JsonInclude(value = JsonInclude.Include.NON_NULL)
    @JsonIgnoreProperties(ignoreUnknown = true)
    private static class Node{
        @JsonProperty("public_uri")
        private String url;

        public String getUrl() {
            return url;
        }

        public void setUrl(String url) {
            this.url = url;
        }

        @Override
        public String toString() {
            return "Node{" +
                    "url='" + url + '\'' +
                    '}';
        }
    }


    @JsonInclude(value = JsonInclude.Include.NON_NULL)
    @JsonIgnoreProperties(ignoreUnknown = true)
    private static class Meta{
        @JsonProperty("submitted_at")
        private String submittedAt;

        public String getSubmittedAt() {
            return submittedAt;
        }

        public void setSubmittedAt(String submittedAt) {
            this.submittedAt = submittedAt;
        }

        @Override
        public String toString() {
            return "Meta{" +
                    "submittedAt='" + submittedAt + '\'' +
                    '}';
        }
    }

    @JsonInclude(value = JsonInclude.Include.NON_NULL)
    @JsonIgnoreProperties(ignoreUnknown = true)
    private static class Hash{
        @JsonProperty("hash_id_node")
        private String hashIdNode;
        private String hash;

        public String getHashIdNode() {
            return hashIdNode;
        }

        public void setHashIdNode(String hashIdNode) {
            this.hashIdNode = hashIdNode;
        }

        public String getHash() {
            return hash;
        }

        public void setHash(String hash) {
            this.hash = hash;
        }

        @Override
        public String toString() {
            return "Hash{" +
                    "hashIdNode='" + hashIdNode + '\'' +
                    ", hash='" + hash + '\'' +
                    '}';
        }
    }
}
