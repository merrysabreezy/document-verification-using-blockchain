/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.anchor;

/**
 *
 * @author DELL
 */
import com.google.common.hash.Hashing;

import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.charset.CharacterCodingException;
import java.nio.charset.Charset;
import java.nio.charset.CharsetEncoder;
import java.util.Random;
import java.util.UUID;
public class StringUtils {
    private static final Charset charset = Charset.forName("UTF-8");
    public static String randomString(){
        return UUID.randomUUID().toString().replace("-","");
    }
    public static String murmur3_128(String data){
        byte[] bytes = getBytes(data);
        if(null == bytes) throw new IllegalArgumentException("Could not convert data into bytes.");
        return Hashing.murmur3_128().hashBytes(bytes).toString();
    }
    public static String sha512(String data){
        byte[] bytes = getBytes(data);
        if(null == bytes) throw new IllegalArgumentException("Could not convert data into bytes.");
        return Hashing.sha512().hashBytes(bytes).toString();
    }
    public static String sha256(byte[] data){
        return Hashing.sha256().hashBytes(data).toString();
    }
    public static String sha256(String data){
        byte[] bytes = getBytes(data);
        if(null == bytes) throw new IllegalArgumentException("Could not convert data into bytes.");
        return sha256(bytes);
    }
    public static String hmacSha512(String secret, byte[] data){
        byte[] key = getBytes(secret);
        if(null == key) throw new IllegalArgumentException("Could not convert key into bytes.");
        return Hashing.hmacSha512(key).hashBytes(data).toString();
    }
    public static String encodeBase64(String text){
        if(null == text){
            return null;
        }
        return java.util.Base64.getEncoder().encodeToString(text.getBytes());
    }
    public static String encodeBase64(byte[] text){
        if(null == text){
            return null;
        }
        return java.util.Base64.getEncoder().encodeToString(text);
    }
    public static String decodeBase64(String base64){
        return new String(java.util.Base64.getDecoder().decode(base64));
    }
    public static byte[] decodeBase64AsBytes(String base64){
        return java.util.Base64.getDecoder().decode(base64);
    }
    public static byte[] decodeBase64AsBytes(byte[] base64){
        return java.util.Base64.getDecoder().decode(base64);
    }
    public static byte[] getBytes(String value){
        CharsetEncoder encoder = charset.newEncoder();
        try {
            ByteBuffer buffer = encoder.encode(CharBuffer.wrap(value));
            byte[] bytes = new byte[buffer.limit()];
            buffer.get(bytes, 0, bytes.length);
            return bytes;
        } catch (CharacterCodingException e) {
            e.printStackTrace();
        }
        return null;
    }
    public static int sixDigitRandomNumber(){
        Random random = new Random();
        int n = random.nextInt(999999);
        if(n >= 100000) return n; else return n+100000;
    }
}
