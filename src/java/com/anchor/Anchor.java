/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.anchor;

/**
 *
 * @author DELL
 */
public interface Anchor {
    AnchorDetail anchor(final String hash);
    String getProof(final AnchorDetail anchorDetail);

    public class AnchorDetail{
        private String nodeId;
        private String nodeHost;
        private long timestamp;
        private boolean anchorStatus;
        private String refId;
        private String proof;

        public AnchorDetail(){
        
         }
        public AnchorDetail(String nodeId,String nodeHost,
                long timestamp,boolean anchorStatus,
                String refId){
            this.nodeId=nodeId;
            this.nodeHost=nodeHost;
            this.timestamp=timestamp;
            this.anchorStatus=anchorStatus;
            this.refId=refId;
        }
        
        public String getProof() {
            return proof;
        }

        public void setProof(String proof) {
            this.proof = proof;
        }
        
        public String getNodeId() {
            return nodeId;
        }

        public void setNodeId(String nodeId) {
            this.nodeId = nodeId;
        }

        public String getNodeHost() {
            return nodeHost;
        }

        public void setNodeHost(String nodeHost) {
            this.nodeHost = nodeHost;
        }

        public long getTimestamp() {
            return timestamp;
        }

        public void setTimestamp(long timestamp) {
            this.timestamp = timestamp;
        }

        public boolean isAnchorStatus() {
            return anchorStatus;
        }

        public void setAnchorStatus(boolean anchorStatus) {
            this.anchorStatus = anchorStatus;
        }

        public String getRefId() {
            return refId;
        }

        public void setRefId(String refId) {
            this.refId = refId;
        }

        @Override
        public String toString() {
            return "AnchorDetail{" +
                    "nodeId='" + nodeId + '\'' +
                    ", nodeHost='" + nodeHost + '\'' +
                    ", timestamp=" + timestamp +
                    ", anchorStatus=" + anchorStatus +
                    ", refId='" + refId + '\'' +
                    '}';
        }
    }
}
