/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.anchor;

/**
 *
 * @author DELL
 */

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;

public final class ThirdPartyHelper {
    private static final ObjectMapper mapper = new ObjectMapper();
    public static <E> E parseJson(TypeReference type, StringBuilder data){
        try {
            return mapper.readValue(data.toString(), type);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }
    public static JsonNode parseJson(StringBuilder data){
        try {
            return mapper.readTree(data.toString());
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }
    static <E> String serialize(E data){
        try {
            return mapper.writeValueAsString(data);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        return null;
    }
    static <E> byte[] serializeAsBytes(E data){
        try {
            return mapper.writeValueAsBytes(data);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        return null;
    }
}
