/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.userdao;

import com.entity.User;

/**
 *
 * @author DELL
 */
public interface UserDao {
    public boolean insertUser(User user);
    public boolean verifyUser(String email,String code);
    public String login(String email,String password);
}
