/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.userdao.impl;

import com.entity.User;
import com.userdao.UserDao;
import com.utils.DBConnection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

/**
 *
 * @author DELL
 */
public class UserDaoImpl implements UserDao {

    DBConnection db = new DBConnection();
    boolean result = false;

    @Override
   public boolean insertUser(User user) {
        result=false;
        
        try {
            PreparedStatement pstmt=db.con.prepareStatement("INSERT INTO USER VALUES(null,?,?,?,?,?,?)");
            pstmt.setString(1,user.getName());
            pstmt.setString(2,user.getEmail());
            pstmt.setString(3,user.getPassword());
            pstmt.setString(4,user.getPhone());
            pstmt.setString(5,user.getCode());
            pstmt.setBoolean(6,user.isVerified());
            if(pstmt.executeUpdate()>0)
            {
                result=true;
            }
            
        } catch (Exception e) {
            result=false;
            e.printStackTrace();
        }
        return result;
    }

    @Override
    public boolean verifyUser(String email, String code) {
        
        result=false;
        try {
            
            PreparedStatement pstmt=db.con.prepareStatement("update user set verified=? where email=? and code=?");
            pstmt.setBoolean(1,true);
            pstmt.setString(2,email);
            pstmt.setString(3,code);
            
            if(pstmt.executeUpdate()>0)
            {
                result=true;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
  }

    @Override
    public String login(String email, String password) {
        PreparedStatement pstmt = null;
        ResultSet resultSet = null;
        try {
            pstmt=db.con.prepareStatement("select * from user where email=? and password=?");
            pstmt.setString(1,email);
            pstmt.setString(2,password);
            resultSet = pstmt.executeQuery();
            if(resultSet.next()){
                boolean isVerified = resultSet.getBoolean("verified");
                if(isVerified){
                    return "successful";
                }
                else{
                    return "notverified";
                }
            }else{
                return "nouser";
            }
        } catch (Exception e) {
            e.printStackTrace();
        }finally{
            if(null != pstmt){
                try{
                    pstmt.close();
                }catch(Exception ignore){}
            }
        }
        return null;
   }

}
