/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.controller;

import com.anchor.Anchor;
import com.anchor.Anchor.AnchorDetail;
import com.anchor.ChainPoint;
import com.anchordao.AnchorDao;
import com.anchordao.impl.AnchorDaoImpl;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author DELL
 */
public class HashServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String hash = request.getParameter("hash");
        AnchorDao ad = new AnchorDaoImpl();
        if(!ad.checkIfExits(hash)){
            ChainPoint chainPoint = new ChainPoint();
            AnchorDetail anchorDetail =  chainPoint.anchor(hash);       
            ad.insertAnchor(anchorDetail);
        }
  }
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
       }
    }

