/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.controller;

import com.anchor.Anchor.AnchorDetail;
import com.anchor.ChainPoint;
import com.anchor.StringUtils;
import com.anchor.ThirdPartyHelper;
import com.anchordao.AnchorDao;
import com.anchordao.impl.AnchorDaoImpl;
import com.fasterxml.jackson.databind.JsonNode;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author DELL
 */
@WebServlet(name = "VerifyServlet", urlPatterns = {"/verify"})
public class VerifyServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
         String hash = request.getParameter("hash");
         AnchorDao anchorDao = new AnchorDaoImpl();
         PrintWriter writer = response.getWriter();
         AnchorDetail anchorDetail = anchorDao.getAnchorDetail(hash);
         if(anchorDetail.getProof() != null){
            String decodeProof = StringUtils.decodeBase64(anchorDetail.getProof());
            System.out.println(decodeProof);
            writer.write("Bitcoin Blockchain Block ID : "+getBlockId(decodeProof));
         }else{
            ChainPoint chainPoint = new ChainPoint();
            String proof = chainPoint.getProof(anchorDetail);
            if(null != proof){
                anchorDao.updateProof(hash, proof);
            String decodeProof = StringUtils.decodeBase64(proof);
            System.out.println(decodeProof);
            writer.write("Bitcoin Blockchain Block ID : "+getBlockId(decodeProof));
            }else{
                System.out.println("Not anchored to bitcoin blockchain.");
                writer.write("Not anchored to bitcoin blockchain.");
            }
         }
   }
    
    private String getBlockId(String proof){
        JsonNode node = ThirdPartyHelper.parseJson(new StringBuilder().append(proof));
                if(null != node){
                    JsonNode arr =  node.get("branches");
                    JsonNode obj = arr.get(0);
                    JsonNode btcArr = obj.get("branches");
                    JsonNode btcObj = btcArr.get(0);
                    if(btcObj.get("label").textValue().equalsIgnoreCase("btc_anchor_branch")){
                        for(JsonNode opNode : btcObj.get("ops")){
                            if(opNode.has("anchors")){
                                return opNode.get("anchors").get(0).get("anchor_id").textValue();
                            }
                        }
                    }
                }
                return null;
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
   }

}
