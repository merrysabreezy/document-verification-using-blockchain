/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.controller;

import com.entity.User;
import com.userdao.UserDao;
import com.userdao.impl.UserDaoImpl;
import com.utils.EmailSender;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author DELL
 */
public class DocServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String act = request.getParameter("act");
        if (act.equals("register")) {
            response.sendRedirect("register.jsp");
        } else if (act.equals("login")) {
            response.sendRedirect("login.jsp");
        }
        else if (act.equals("homepage")) {
            response.sendRedirect("homepage.jsp");
        }
        else if(act.equals("upload")){
            response.sendRedirect("upload.jsp");
        }
        else if(act.equals("logout"))
        {
            request.getSession().invalidate();
            response.sendRedirect("homepage.jsp");
        }
        else if (act.equals("docverify")) {
            response.sendRedirect("docverify.jsp");
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        EmailSender es = new EmailSender();
        UserDao udao = new UserDaoImpl();
        String act = request.getParameter("act");
        if (act.equals("register")) {
            User user = new User(0, request.getParameter("name"), request.getParameter("email"), request.getParameter("password"), request.getParameter("phone"), "1", false);
            String code = es.generateCode();
            user.setCode(code);
            if (es.sendEmail(code, user.getEmail())) {
                if (udao.insertUser(user)) {
                    response.sendRedirect("verify.jsp?msg=Enter the code you got in email&email=" + user.getEmail());
                } else {
                    response.sendRedirect("register.jsp?act=registration failed");

                }
            } else {
                response.sendRedirect("register.jsp?act=registration failed");
            }
        }
        
        else if(act.equals("verify"))
        {
            String email=request.getParameter("email");
            String code=request.getParameter("code");
            if(udao.verifyUser(email, code))
            {
                response.sendRedirect("homepage.jsp");
            }
            else{
                response.sendRedirect("verify.jsp?msg=invalid code&email="+email);
            }    
        }
        else if(act.equals("login"))
        {
            String email=request.getParameter("email");
            String password=request.getParameter("password");
            String result=udao.login(email, password);
            System.out.println(result);
            
            if(result.equals("successful")){
                request.getSession().setAttribute("userSession", email);
                response.sendRedirect("upload.jsp");
            }
            else if(result.equals("notverified"))
            {
                response.sendRedirect("verify.jsp?email="+email);
            }
            else if(result.equals("nouser"))
            {
                response.sendRedirect("register.jsp?msg=please register");
            }
        }
    }

}
