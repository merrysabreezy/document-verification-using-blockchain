/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
(function () {
    function $id(id) {
        return document.getElementById(id);
    }
    // output information
    function Output(msg) {
        var m = $id("fileDetail");
        m.innerHTML = msg;
    }
    function OutputHash(eId, msg) {
        $("#" + eId + " .hash").html(msg);
    }
    // file drag hover
    function FileDragHover(e) {
        e.stopPropagation();
        e.preventDefault();
        e.target.className = (e.type == "dragover" ? "hover" : "");
    }
    // file selection
    function FileSelectHandler(e) {
        // cancel event and hover styling
        FileDragHover(e);
        // fetch FileList object
        var files = e.target.files || e.dataTransfer.files;
        // process all File objects
        for (var i = 0, f; f = files[i]; i++) {
            ParseFile(f, i);
        }

    }
    function sha256_str(str) {
        // We transform the string into an arraybuffer.
        var buffer = new TextEncoder("utf-8").encode(str);
        return crypto.subtle.digest("SHA-256", buffer).then(function (hash) {
          return hex(hash);
        });
    }
    function sha256(buffer) {
        return crypto.subtle.digest("SHA-256", buffer).then(function (hash) {
          return hex(hash);
        });
    }

    function hex(buffer) {
      var hexCodes = [];
      var view = new DataView(buffer);
      for (var i = 0; i < view.byteLength; i += 4) {
        // Using getUint32 reduces the number of iterations needed (we process 4 bytes each time)
        var value = view.getUint32(i);
        // toString(16) will give the hex representation of the number without padding
        var stringValue = value.toString(16);
        // We use concatenation and slice for padding
        var padding = '00000000';
        var paddedValue = (padding + stringValue).slice(-padding.length)
        hexCodes.push(paddedValue);
      }
      // Join all the hex strings into one
      return hexCodes.join("");
    }

    // output file information
    function ParseFile(file, id) {
        var fileReader = new FileReader;
        fileReader.onload = function (f) {      
        sha256(f.target.result).then(function(digest) {
           sha256_str(digest).then(function(hs){
               $("#messages").html(hs);
               //Send serverusing ajax
               $.get( "http://localhost:8084/DocumentVerification/verify?hash="+hs, 
               function( data ) {
               //Show msg
               Output(data);
               console.log("Anchor done.");
              });
               
           }); 
        });
        };
        fileReader.readAsArrayBuffer(file);
        Output(
                "<p id='" + id + "'>File information: <strong>" + file.name +
                "</strong> type: <strong>" + file.type +
                "</strong> size: <strong>" + file.size +
                "</strong> <span style='color:green;'>hash: <strong class='hash'>" +
                "</strong></span> " +
                "</p>"
                );
   
    }
    // initialize
    function Init() {

        var fileselect = $id("fileselect"),
                filedrag = $id("filedrag"),
                submitbutton = $id("submitbutton");
        // file select
        fileselect.addEventListener("change", FileSelectHandler, false);
        // is XHR2 available?
        var xhr = new XMLHttpRequest();
        if (xhr.upload) {

            // file drop
            filedrag.addEventListener("dragover", FileDragHover, false);
            filedrag.addEventListener("dragleave", FileDragHover, false);
            filedrag.addEventListener("drop", FileSelectHandler, false);
            filedrag.style.display = "block";
            // remove submit button
            submitbutton.style.display = "none";
        }

    }

    // call initialization file
    if (window.File && window.FileList && window.FileReader) {
        Init();
    }
})
        ();

