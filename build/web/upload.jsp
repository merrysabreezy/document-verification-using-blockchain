<%-- 
    Document   : hash3
    Created on : Nov 2, 2018, 2:15:27 PM
    Author     : DELL
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html lang="en" dir="ltr">
    <head>
        <meta charset="UTF-8" />
        <title>Upload</title>
        <link rel="stylesheet" type="text/css" media="all" href="assets/css/styles.css" />
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">

        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script> 
    </head>
    <body>
         <nav class="navbar navbar-expand-lg navbar navbar-dark bg-dark">
            <a class="navbar-brand" href="doc?act=homepage">
                <img src="assets/images/whitelogo.png" width="50" height="30" alt="">
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav mr-auto">
                 
                    <li class="nav-item active">
                        <a class="nav-link" href="doc?act=docverify">Verify </a>
                    </li>

                </ul>

            </div>
        </nav>
    <center>
        <h1 style="color: #FFF; font-family: initial"> <b>Welcome to Doc Certify</b> </h1>
        <p style="color: #FFF; font-family: initial"> Select your document and have it certified </p>
    </center>
    <form id="upload" action="upload.php" method="POST" enctype="multipart/form-data">

        <fieldset>

            <input type="hidden" id="MAX_FILE_SIZE" name="MAX_FILE_SIZE" value="300000" />

            <div>
                <input type="file" id="fileselect" name="fileselect[]" multiple="multiple" />
                <div id="filedrag">or drop files here</div>
            </div>

            <div id="submitbutton">
                <button type="submit">Upload Files</button>
            </div>

        </fieldset>

    </form>
    <div id="fileDetail" style="color: #FFF">
    </div>
    <div id="messages" style="color: #FFF">
    </div>
  

   
    
    <script type="text/javascript" src="assets/js/hashme.js"></script>
    <script type="text/javascript" src="assets/js/filedrag.js"></script>
    <script type="text/javascript" src="assets/js/md5.js"></script>

</body>
</html>
