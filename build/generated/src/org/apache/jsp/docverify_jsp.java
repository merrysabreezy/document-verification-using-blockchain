package org.apache.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;

public final class docverify_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List<String> _jspx_dependants;

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public java.util.List<String> getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html;charset=UTF-8");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("<!DOCTYPE html>\n");
      out.write("<html>\n");
      out.write("    <head>\n");
      out.write("       <meta charset=\"UTF-8\" />\n");
      out.write("        <title>Verify</title>\n");
      out.write("        <link rel=\"stylesheet\" type=\"text/css\" media=\"all\" href=\"assets/css/styles.css\" />\n");
      out.write("        <script src=\"//ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js\"></script> \n");
      out.write("    </head>\n");
      out.write("    <body>\n");
      out.write("    <center>\n");
      out.write("        <h1 style=\"color: #FFF\"> <b>Welcome to Doc Certify</b> </h1>\n");
      out.write("        <p style=\"color: #FFF\"> Select your document and get it verified.</p>\n");
      out.write("    </center>\n");
      out.write("    <form id=\"upload\" action=\"upload.php\" method=\"POST\" enctype=\"multipart/form-data\">\n");
      out.write("\n");
      out.write("        <fieldset>\n");
      out.write("\n");
      out.write("            <input type=\"hidden\" id=\"MAX_FILE_SIZE\" name=\"MAX_FILE_SIZE\" value=\"300000\" />\n");
      out.write("\n");
      out.write("            <div>\n");
      out.write("                <input type=\"file\" id=\"fileselect\" name=\"fileselect[]\" multiple=\"multiple\" />\n");
      out.write("                <div id=\"filedrag\">or drop files here</div>\n");
      out.write("            </div>\n");
      out.write("\n");
      out.write("            <div id=\"submitbutton\">\n");
      out.write("                <button type=\"submit\">Upload Files</button>\n");
      out.write("            </div>\n");
      out.write("\n");
      out.write("        </fieldset>\n");
      out.write("\n");
      out.write("    </form>\n");
      out.write("    <div id=\"fileDetail\">\n");
      out.write("    </div>\n");
      out.write("    <div id=\"messages\">\n");
      out.write("    </div>\n");
      out.write("    <script type=\"text/javascript\" src=\"assets/js/hashme.js\"></script>\n");
      out.write("    <script type=\"text/javascript\" src=\"assets/js/filedragverify.js\"></script>\n");
      out.write("    <script type=\"text/javascript\" src=\"assets/js/md5.js\"></script>\n");
      out.write("</body>\n");
      out.write("</html>\n");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
