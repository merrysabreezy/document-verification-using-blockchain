package org.apache.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;

public final class navbar_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List<String> _jspx_dependants;

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public java.util.List<String> getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html;charset=UTF-8");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("<!DOCTYPE html>\n");
      out.write("<html>\n");
      out.write("<head>\n");
      out.write("<meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">\n");
      out.write("<link href=\"//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css\" rel=\"stylesheet\" id=\"bootstrap-css\">\n");
      out.write("<script src=\"//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js\"></script>\n");
      out.write("<script src=\"//code.jquery.com/jquery-1.11.1.min.js\"></script>\n");
      out.write("</head>\n");
      out.write("<body>\n");
      out.write("    <nav id=\"header\" class=\"navbar navbar-fixed-top\">\n");
      out.write("            <div id=\"header-container\" class=\"container navbar-container\">\n");
      out.write("                <div class=\"navbar-header\">\n");
      out.write("                    <button type=\"button\" class=\"navbar-toggle collapsed\" data-toggle=\"collapse\" data-target=\"#navbar\" aria-expanded=\"false\" aria-controls=\"navbar\">\n");
      out.write("                        <span class=\"sr-only\">Toggle navigation</span>\n");
      out.write("                        <span class=\"icon-bar\"></span>\n");
      out.write("                        <span class=\"icon-bar\"></span>\n");
      out.write("                        <span class=\"icon-bar\"></span>\n");
      out.write("                    </button>\n");
      out.write("                    <a id=\"brand\" class=\"navbar-brand\" href=\"#\">Project name</a>\n");
      out.write("                </div>\n");
      out.write("                <div id=\"navbar\" class=\"collapse navbar-collapse\">\n");
      out.write("                    <ul class=\"nav navbar-nav\">\n");
      out.write("                        <li class=\"active\"><a href=\"#\">Home</a></li>\n");
      out.write("                        <li><a href=\"#about\">About</a></li>\n");
      out.write("                        <li><a href=\"#contact\">Contact</a></li>\n");
      out.write("                    </ul>\n");
      out.write("                </div><!-- /.nav-collapse -->\n");
      out.write("            </div><!-- /.container -->\n");
      out.write("        </nav><!-- /.navbar -->\n");
      out.write("\n");
      out.write("        <div class=\"container\">\n");
      out.write("\n");
      out.write("            <div class=\"row row-offcanvas row-offcanvas-right\">\n");
      out.write("\n");
      out.write("                <div class=\"col-xs-12 col-sm-9\">\n");
      out.write("                    <p class=\"pull-right visible-xs\">\n");
      out.write("                        <button type=\"button\" class=\"btn btn-primary btn-xs\" data-toggle=\"offcanvas\">Toggle nav</button>\n");
      out.write("                    </p>\n");
      out.write("                    <div class=\"jumbotron\">\n");
      out.write("                        <h1>Hello, world!</h1>\n");
      out.write("                        <p>This is an example to show the potential of an offcanvas layout pattern in Bootstrap. Try some responsive-range viewport sizes to see it in action.</p>\n");
      out.write("                    </div>\n");
      out.write("                    <div class=\"row\">\n");
      out.write("                        <div class=\"col-xs-6 col-lg-4\">\n");
      out.write("                            <h2>Heading</h2>\n");
      out.write("                            <p>Donec id elit non mi porta gravida at eget metus. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus. Etiam porta sem malesuada magna mollis euismod. Donec sed odio dui. </p>\n");
      out.write("                            <p><a class=\"btn btn-default\" href=\"#\" role=\"button\">View details »</a></p>\n");
      out.write("                        </div><!--/.col-xs-6.col-lg-4-->\n");
      out.write("                        <div class=\"col-xs-6 col-lg-4\">\n");
      out.write("                            <h2>Heading</h2>\n");
      out.write("                            <p>Donec id elit non mi porta gravida at eget metus. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus. Etiam porta sem malesuada magna mollis euismod. Donec sed odio dui. </p>\n");
      out.write("                            <p><a class=\"btn btn-default\" href=\"#\" role=\"button\">View details »</a></p>\n");
      out.write("                        </div><!--/.col-xs-6.col-lg-4-->\n");
      out.write("                        <div class=\"col-xs-6 col-lg-4\">\n");
      out.write("                            <h2>Heading</h2>\n");
      out.write("                            <p>Donec id elit non mi porta gravida at eget metus. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus. Etiam porta sem malesuada magna mollis euismod. Donec sed odio dui. </p>\n");
      out.write("                            <p><a class=\"btn btn-default\" href=\"#\" role=\"button\">View details »</a></p>\n");
      out.write("                        </div><!--/.col-xs-6.col-lg-4-->\n");
      out.write("                        <div class=\"col-xs-6 col-lg-4\">\n");
      out.write("                            <h2>Heading</h2>\n");
      out.write("                            <p>Donec id elit non mi porta gravida at eget metus. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus. Etiam porta sem malesuada magna mollis euismod. Donec sed odio dui. </p>\n");
      out.write("                            <p><a class=\"btn btn-default\" href=\"#\" role=\"button\">View details »</a></p>\n");
      out.write("                        </div><!--/.col-xs-6.col-lg-4-->\n");
      out.write("                        <div class=\"col-xs-6 col-lg-4\">\n");
      out.write("                            <h2>Heading</h2>\n");
      out.write("                            <p>Donec id elit non mi porta gravida at eget metus. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus. Etiam porta sem malesuada magna mollis euismod. Donec sed odio dui. </p>\n");
      out.write("                            <p><a class=\"btn btn-default\" href=\"#\" role=\"button\">View details »</a></p>\n");
      out.write("                        </div><!--/.col-xs-6.col-lg-4-->\n");
      out.write("                        <div class=\"col-xs-6 col-lg-4\">\n");
      out.write("                            <h2>Heading</h2>\n");
      out.write("                            <p>Donec id elit non mi porta gravida at eget metus. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus. Etiam porta sem malesuada magna mollis euismod. Donec sed odio dui. </p>\n");
      out.write("                            <p><a class=\"btn btn-default\" href=\"#\" role=\"button\">View details »</a></p>\n");
      out.write("                        </div><!--/.col-xs-6.col-lg-4-->\n");
      out.write("                    </div><!--/row-->\n");
      out.write("                </div><!--/.col-xs-12.col-sm-9-->\n");
      out.write("\n");
      out.write("                <div class=\"col-xs-6 col-sm-3 sidebar-offcanvas\" id=\"sidebar\">\n");
      out.write("                    <div class=\"list-group\">\n");
      out.write("                        <a href=\"#\" class=\"list-group-item active\">Link</a>\n");
      out.write("                        <a href=\"#\" class=\"list-group-item\">Link</a>\n");
      out.write("                        <a href=\"#\" class=\"list-group-item\">Link</a>\n");
      out.write("                        <a href=\"#\" class=\"list-group-item\">Link</a>\n");
      out.write("                        <a href=\"#\" class=\"list-group-item\">Link</a>\n");
      out.write("                        <a href=\"#\" class=\"list-group-item\">Link</a>\n");
      out.write("                        <a href=\"#\" class=\"list-group-item\">Link</a>\n");
      out.write("                        <a href=\"#\" class=\"list-group-item\">Link</a>\n");
      out.write("                        <a href=\"#\" class=\"list-group-item\">Link</a>\n");
      out.write("                        <a href=\"#\" class=\"list-group-item\">Link</a>\n");
      out.write("                    </div>\n");
      out.write("                </div><!--/.sidebar-offcanvas-->\n");
      out.write("            </div><!--/row-->\n");
      out.write("\n");
      out.write("            <hr>\n");
      out.write("\n");
      out.write("            <footer>\n");
      out.write("                <p>© Company 2014</p>\n");
      out.write("            </footer>\n");
      out.write("\n");
      out.write("        </div><!--/.container-->\n");
      out.write("\n");
      out.write("</body>\n");
      out.write("</html>\n");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
