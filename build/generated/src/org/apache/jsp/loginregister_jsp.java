package org.apache.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;

public final class loginregister_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List<String> _jspx_dependants;

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public java.util.List<String> getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html;charset=UTF-8");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("<!DOCTYPE html>\n");
      out.write("<html>\n");
      out.write("    <head>\n");
      out.write("        <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">\n");
      out.write("        <title>JSP Page</title>\n");
      out.write("    </head>\n");
      out.write("    <body>\n");
      out.write("        <!--Modal: Login / Register Form-->\n");
      out.write("<div class=\"modal fade\" id=\"modalLRForm\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"myModalLabel\" aria-hidden=\"true\">\n");
      out.write("  <div class=\"modal-dialog cascading-modal\" role=\"document\">\n");
      out.write("    <!--Content-->\n");
      out.write("    <div class=\"modal-content\">\n");
      out.write("\n");
      out.write("      <!--Modal cascading tabs-->\n");
      out.write("      <div class=\"modal-c-tabs\">\n");
      out.write("\n");
      out.write("        <!-- Nav tabs -->\n");
      out.write("        <ul class=\"nav nav-tabs md-tabs tabs-2 light-blue darken-3\" role=\"tablist\">\n");
      out.write("          <li class=\"nav-item\">\n");
      out.write("            <a class=\"nav-link active\" data-toggle=\"tab\" href=\"#panel7\" role=\"tab\"><i class=\"fa fa-user mr-1\"></i>\n");
      out.write("              Login</a>\n");
      out.write("          </li>\n");
      out.write("          <li class=\"nav-item\">\n");
      out.write("            <a class=\"nav-link\" data-toggle=\"tab\" href=\"#panel8\" role=\"tab\"><i class=\"fa fa-user-plus mr-1\"></i>\n");
      out.write("              Register</a>\n");
      out.write("          </li>\n");
      out.write("        </ul>\n");
      out.write("\n");
      out.write("        <!-- Tab panels -->\n");
      out.write("        <div class=\"tab-content\">\n");
      out.write("          <!--Panel 7-->\n");
      out.write("          <div class=\"tab-pane fade in show active\" id=\"panel7\" role=\"tabpanel\">\n");
      out.write("\n");
      out.write("            <!--Body-->\n");
      out.write("            <div class=\"modal-body mb-1\">\n");
      out.write("              <div class=\"md-form form-sm mb-5\">\n");
      out.write("                <i class=\"fa fa-envelope prefix\"></i>\n");
      out.write("                <input type=\"email\" id=\"modalLRInput10\" class=\"form-control form-control-sm validate\">\n");
      out.write("                <label data-error=\"wrong\" data-success=\"right\" for=\"modalLRInput10\">Your email</label>\n");
      out.write("              </div>\n");
      out.write("\n");
      out.write("              <div class=\"md-form form-sm mb-4\">\n");
      out.write("                <i class=\"fa fa-lock prefix\"></i>\n");
      out.write("                <input type=\"password\" id=\"modalLRInput11\" class=\"form-control form-control-sm validate\">\n");
      out.write("                <label data-error=\"wrong\" data-success=\"right\" for=\"modalLRInput11\">Your password</label>\n");
      out.write("              </div>\n");
      out.write("              <div class=\"text-center mt-2\">\n");
      out.write("                <button class=\"btn btn-info\">Log in <i class=\"fa fa-sign-in ml-1\"></i></button>\n");
      out.write("              </div>\n");
      out.write("            </div>\n");
      out.write("            <!--Footer-->\n");
      out.write("            <div class=\"modal-footer\">\n");
      out.write("              <div class=\"options text-center text-md-right mt-1\">\n");
      out.write("                <p>Not a member? <a href=\"#\" class=\"blue-text\">Sign Up</a></p>\n");
      out.write("                <p>Forgot <a href=\"#\" class=\"blue-text\">Password?</a></p>\n");
      out.write("              </div>\n");
      out.write("              <button type=\"button\" class=\"btn btn-outline-info waves-effect ml-auto\" data-dismiss=\"modal\">Close</button>\n");
      out.write("            </div>\n");
      out.write("\n");
      out.write("          </div>\n");
      out.write("          <!--/.Panel 7-->\n");
      out.write("\n");
      out.write("          <!--Panel 8-->\n");
      out.write("          <div class=\"tab-pane fade\" id=\"panel8\" role=\"tabpanel\">\n");
      out.write("\n");
      out.write("            <!--Body-->\n");
      out.write("            <div class=\"modal-body\">\n");
      out.write("              <div class=\"md-form form-sm mb-5\">\n");
      out.write("                <i class=\"fa fa-envelope prefix\"></i>\n");
      out.write("                <input type=\"email\" id=\"modalLRInput12\" class=\"form-control form-control-sm validate\">\n");
      out.write("                <label data-error=\"wrong\" data-success=\"right\" for=\"modalLRInput12\">Your email</label>\n");
      out.write("              </div>\n");
      out.write("\n");
      out.write("              <div class=\"md-form form-sm mb-5\">\n");
      out.write("                <i class=\"fa fa-lock prefix\"></i>\n");
      out.write("                <input type=\"password\" id=\"modalLRInput13\" class=\"form-control form-control-sm validate\">\n");
      out.write("                <label data-error=\"wrong\" data-success=\"right\" for=\"modalLRInput13\">Your password</label>\n");
      out.write("              </div>\n");
      out.write("\n");
      out.write("              <div class=\"md-form form-sm mb-4\">\n");
      out.write("                <i class=\"fa fa-lock prefix\"></i>\n");
      out.write("                <input type=\"password\" id=\"modalLRInput14\" class=\"form-control form-control-sm validate\">\n");
      out.write("                <label data-error=\"wrong\" data-success=\"right\" for=\"modalLRInput14\">Repeat password</label>\n");
      out.write("              </div>\n");
      out.write("\n");
      out.write("              <div class=\"text-center form-sm mt-2\">\n");
      out.write("                <button class=\"btn btn-info\">Sign up <i class=\"fa fa-sign-in ml-1\"></i></button>\n");
      out.write("              </div>\n");
      out.write("\n");
      out.write("            </div>\n");
      out.write("            <!--Footer-->\n");
      out.write("            <div class=\"modal-footer\">\n");
      out.write("              <div class=\"options text-right\">\n");
      out.write("                <p class=\"pt-1\">Already have an account? <a href=\"#\" class=\"blue-text\">Log In</a></p>\n");
      out.write("              </div>\n");
      out.write("              <button type=\"button\" class=\"btn btn-outline-info waves-effect ml-auto\" data-dismiss=\"modal\">Close</button>\n");
      out.write("            </div>\n");
      out.write("          </div>\n");
      out.write("          <!--/.Panel 8-->\n");
      out.write("        </div>\n");
      out.write("\n");
      out.write("      </div>\n");
      out.write("    </div>\n");
      out.write("    <!--/.Content-->\n");
      out.write("  </div>\n");
      out.write("</div>\n");
      out.write("<!--Modal: Login / Register Form-->\n");
      out.write("\n");
      out.write("<div class=\"text-center\">\n");
      out.write("  <a href=\"\" class=\"btn btn-default btn-rounded my-3\" data-toggle=\"modal\" data-target=\"#modalLRForm\">Launch\n");
      out.write("    Modal LogIn/Register</a>\n");
      out.write("</div>\n");
      out.write("    </body>\n");
      out.write("</html>\n");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
