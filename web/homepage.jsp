<%-- 
    Document   : homepage
    Created on : Oct 6, 2018, 4:51:53 PM
    Author     : DELL
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Doc Certify</title>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">

        <style>
            body {
                font-family: 'Varela Round', sans-serif;
                background-image: url("assets/images/graybgtext_1.jpg");
                background-size: 100%;
            }
            p{
                font-family: cursive;
                font-style: inherit;
                color: cornsilk;
            }
            .modal-login {
                width: 350px;
            }
            .modal-login .modal-content {
                padding: 20px;
                border-radius: 5px;
                border: none;
            }
            .modal-login .modal-header {
                border-bottom: none;
                position: relative;
                justify-content: center;
            }
            .modal-login .close {
                position: absolute;
                top: -10px;
                right: -10px;
            }
            .modal-login h4 {
                color: #636363;
                text-align: center;
                font-size: 26px;
                margin-top: 0;
            }
            .modal-login .modal-content {
                color: #999;
                border-radius: 1px;
                margin-bottom: 15px;
                background: #fff;
                border: 1px solid #f3f3f3;
                box-shadow: 0px 2px 2px rgba(0, 0, 0, 0.3);
                padding: 25px;
            }
            .modal-login .form-group {
                margin-bottom: 20px;
            }
            .modal-login label {
                font-weight: normal;
                font-size: 13px;
            }
            .modal-login .form-control {
                min-height: 38px;
                padding-left: 5px;
                box-shadow: none !important;
                border-width: 0 0 1px 0;
                border-radius: 0;
            }
            .modal-login .form-control:focus {
                border-color: #ccc;
            }
            .modal-login .input-group-addon {
                max-width: 42px;
                text-align: center;
                background: none;
                border-width: 0 0 1px 0;
                padding-left: 5px;
                border-radius: 0;
            }
            .modal-login .btn {        
                font-size: 16px;
                font-weight: bold;
                background: #19aa8d;
                border-radius: 3px;
                border: none;
                min-width: 140px;
                outline: none !important;
            }
            .modal-login .btn:hover, .modal-login .btn:focus {
                background: #179b81;
            }
            .modal-login .hint-text {
                text-align: center;
                padding-top: 5px;
                font-size: 13px;
            }
            .modal-login .modal-footer {
                color: #999;
                border-color: #dee4e7;
                text-align: center;
                margin: 0 -25px -25px;
                font-size: 13px;
                justify-content: center;
            }
            .modal-login a {
                color: #fff;
                text-decoration: underline;
            }
            .modal-login a:hover {
                text-decoration: none;
            }
            .modal-login a {
                color: #19aa8d;
                text-decoration: none;
            }	
            .modal-login a:hover {
                text-decoration: underline;
            }
            .modal-login .fa {
                font-size: 21px;
            }
            .trigger-btn {
                display: inline-block;
                margin: 100px auto;
            }
        </style>
    </head>
    <body>
    <center>
        <nav class="navbar navbar-expand-lg navbar navbar-dark bg-dark ">
            <a class="navbar-brand" href="doc?act=homepage">
                <img src="assets/images/whitelogo.png" width="50" height="30" alt="" >
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav mr-auto">

                    <li class="nav-item active">
                        
                        <!-- Button trigger modal -->
                        <button type="button" class="btn btn-dark" data-toggle="modal" data-target="#exampleModalCenter">
                            About us
                        </button>
                      

                        <!-- Modal -->
                        <div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                            <div class="modal-dialog modal-dialog-centered" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="exampleModalCenterTitle"> A web application to prove the existence of documents using the blockchain</h5>


                                    </div>
                                    <div class="modal-body">

                                        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

                                        <div class="container">


                                            <div class="box">
                                                <div class="icon"><i class="fa fa-envelope" aria-hidden="true"></i></div>
                                                <div class='details'><h3>doccertify@gmail.com</h3></div>
                                            </div>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>

                    </li>
                    <li class="nav-item active">
                        <!-- Button HTML (to Trigger Modal) -->
                        <a href="#myModal" class="nav-link" data-toggle="modal">Login</a>
                        <!-- Modal HTML -->
                        <div id="myModal" class="modal fade">
                            <div class="modal-dialog modal-login">
                                <div class="modal-content">
                                    <div class="modal-header">				
                                        <h4 class="modal-title">Sign In</h4>
                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                    </div>
                                    <div class="modal-body">
                                        <form action="doc?act=login" method="post">
                                            <div class="form-group">
                                                <div class="input-group">
                                                    <span class="input-group-addon"><i class="fa fa-envelope fa"></i></span>
                                                    <input type="email" class="form-control" name="email" placeholder="Email" required="required">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="input-group">
                                                    <span class="input-group-addon"><i class="fa fa-lock"></i></span>
                                                    <input type="password" class="form-control" name="password" placeholder="Password" required="required">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <button type="submit" class="btn btn-primary btn-block btn-lg">Sign In</button>
                                            </div>
                                        </form>
                                    </div>
                                    <div class="modal-footer">Don't have an account? <a href="doc?act=register">Create one</a></div>
                                </div>
                            </div>
                        </div> 
                    </li>  
                    <li class="nav-item active">
                        <a class="nav-link" href="doc?act=register">Register</a>
                    </li>
                    

                </ul>

            </div>
        </nav>

        <br><br><br>

        <div class="text-center">
            <img src="assets/images/whitelogo.png" class="rounded" width="300" height="150" alt="...">
        </div>

        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
    </center>
    
</body>
</html>
